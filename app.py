from flask import Flask
from pyswip import Prolog

app = Flask(__name__)


@app.route('/')
def hello_world():
    # Menjalankan fungsi prolog. Sesuaikan input berdasarkan web app.
    prolog = Prolog()

    # TODO: Manipulasi input untuk memeriksa konflik jadwal.
    return 'Hello World!'


if __name__ == '__main__':
    app.run()
