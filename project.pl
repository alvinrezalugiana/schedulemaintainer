% Tugas kelompok Logic Jos - SusunJadwal 2.0
% Logika program: Menggunakan informasi kelas untuk memeriksa adanya
% konflik jadwal.

% Hal baru yang dipelajari dalam pengerjaan proyek:
% 1. Kontrol input (Menghindari input non-atomik).
%    Input variabel dan angka harus dikontrol guna menghindari error.
% 2. Kontrol konstrain.
%    Logika penjadwalan harus disesuaikan untuk menghindari konflik
%    jadwal.
% 3. Looping dalam Prolog
%    Menggunakan predikat forall/2.

% Logika inti program: Menambahkan informasi kelas, memeriksa terjadinya
% konflik.

isiJadwal :-
    write("Mata Pelajaran (Contoh: pemlog.): "), read(Kelas),
    cekAtom(Kelas), atom_string(Kelas, NamaKelas),

    write("Hari (Contoh: senin.): "), read(Hari),
    cekAtom(Hari), atom_string(Hari, Day),cekHari(Day),

    write("Jam Kelas Mulai (Contoh: 08.00): "), read(Mulai),
    cekFloat(Mulai), cekMulai(Mulai),

    write("Jam Kelas Selesai (Contoh: 13.50): "), read(Selesai),
    cekFloat(Selesai), cekSelesai(Mulai, Selesai),

    % Hitung jumlah elemen list.
    findall(_, infoKelas(_,_,_,_), L),
    jumlahElemen(L, Count),
    kondisiKelas(Count, NamaKelas, Day, Mulai, Selesai),

    write("Adakah kelas lain yang diambil?(ya/tidak)"), read(Prompt),
    kondisi(Prompt).


% Predikat kontrol untuk exception handling.
kontrol :- catch(isiJadwal, X, prosesError(X)).

% Predikat cek untuk memeriksa input; mengeluarkan exception jika tidak
% sesuai.
cekAtom(X) :- atom(X), !.
cekAtom(_) :- throw(notAtom).

cekFloat(X) :- float(X), !.
cekFloat(_) :- throw(notFloat).

cekHari(X) :- hari(X), !.
cekHari(_) :- throw(notHari).

cekMulai(X) :- jam_mulai(X), !.
cekMulai(_) :- throw(notMulai).

cekSelesai(Mulai, Selesai) :- Selesai > Mulai, jam_selesai(Selesai), !.
cekSelesai(_, _) :- throw(notSelesai).

cekKelas(_, Hari, M2, S2) :-
    forall(bagof(_, infoKelas(_, Hari, M1, S1), _),
    cekJam(M1, S1, M2, S2)).
cekKelas(_,_,_,_) :- throw(errorKelas).

cekJam(M1, S1, M2, S2):-
    % Case 1: Kelas kedua selesai saat kelas pertama berlangsung.
    M1 > M2, S2 > M1, S2 < S1, !, fail.

cekJam(M1, S1, M2, S2):-
    % Case 2: Kelas kedua mulai sebelum kelas pertama mulai,
    % selesai setelah kelas pertama selesai.
    M1 > M2, S2 > M1, S2 > S1, !, fail.

cekJam(_, S1, _, S2):-
    % Case 3: Kedua kelas selesai secara bersamaan, fail.
    S1 == S2, !, fail.

cekJam(M1, S1, M2, _):-
    % Case 4: Kelas kedua mulai dan selesai SEBELUM Kelas pertama.
    M1 > M2, S1 > M2, !.

cekJam(M1, S1, M2, S2):-
    % Case 5: Kelas kedua mulai saat kelas pertama berlangsung.
    M1 < M2, M2 < S1, S2 > S1, !, fail.

cekJam(M1, S1, M2, _):-
    % Case 6: Kelas kedua mulai SETELAH Kelas pertama selesai.
    M1 < M2, S1 < M2, !.

cekJam(M1, _, M2, _):-
    % Case 7: Kedua kelas dimulai secara bersamaan, fail.
    M1 == M2, !, fail.

kondisiKelas(Count, NamaKelas, Hari, Mulai, Selesai) :-
    Count >= 1, cekKelas(NamaKelas, Hari, Mulai, Selesai),
    assert(infoKelas(NamaKelas, Hari, Mulai, Selesai)), !.

kondisiKelas(Count, NamaKelas, Hari, Mulai, Selesai) :-
    Count == 0, assert(infoKelas(NamaKelas, Hari, Mulai, Selesai)), !.




% Predikat proses untuk memproses tiap error yang terjadi.
prosesError(notAtom) :-
    write("Input bukan atom (angka, variabel dll.).\n"),
    write("Mengulang proses input dari awal.\n"),
    nl, kontrol, !.

prosesError(notFloat) :-
    write("Input jam tidak sesuai.\n"),
    write("Mengulang proses input dari awal.\n"),
    nl, kontrol, !.

prosesError(notHari) :-
    write("Input bukan hari.\n"),
    write("Mengulang proses input dari awal.\n"),
    nl, kontrol, !.

prosesError(notMulai) :-
    write("Input jam tidak sesuai jadwal normal.\n"),
    write("Mengulang proses input dari awal.\n"),
    nl, kontrol, !.

prosesError(notSelesai) :-
    write("Input jam tidak sesuai jadwal normal.\n"),
    write("Mengulang proses input dari awal.\n"),
    nl, kontrol, !.


prosesError(errorKelas) :-
    write("Input kelas tidak memungkinkan.\n"),
    write("Mengulang proses input dari awal.\n"),
    nl, kontrol, !.


% Menggunakan predikat dinamis infoKelas guna meningkatkan fleksibilitas
% program.
:- dynamic infoKelas/4.

% Membuka program, menyalakan kontrol.
go :-
    write('Selamat datang di program SusunJadwal2.0!\n'),
    write('Silahkan memasukkan jadwal pelajaran anda sesuai permintaan program!\n'),
    write('CATATAN PENTING: Harap menggunakan lower-case dikarenakan keterbatasan penggunaan Prolog. Tambahkan juga "." setelah memasukkan input.\n'), nl, kontrol.



% Predikat menghitung jumlah elemen sebuah list.
jumlahElemen([],0).
jumlahElemen([_|T],N) :- jumlahElemen(T,N1) , N is N1+1.

% Kondisi prompt untuk menambah kelas atau selesai menggunakan program.
kondisi(Prompt) :-
    Prompt == ya, isiJadwal, !.

kondisi(Prompt) :-
    Prompt == tidak, prosesJadwal, !.

prosesJadwal :-
    % Mengambil seluruh elemen untuk print out hasil.
    findall(K, infoKelas(K,_,_,_), List_Kelas),
    findall(H, infoKelas(_,H,_,_), List_Hari),
    findall(M, infoKelas(_,_,M,_), List_Mulai),
    findall(S, infoKelas(_,_,_,S), List_Selesai),
    Count is 1,

    nl, write("Terima kasih sudah menggunakan SusunJadwal2.0!"), nl,
    write("Berikut adalah jadwal yang telah dibuat:"), nl,
    write("No."), tab(4), write("Nama Kelas"), tab(6), write("Hari"), tab(6),
    write("Jam Mulai"), tab(6),
    write("Jam Selesai"), nl,
    cetakJadwal(Count, List_Kelas, List_Hari, List_Mulai, List_Selesai).

% Mencetak jadwal.
cetakJadwal(_, [], _, _, _).
cetakJadwal(Count, [Lk|T1], [Lh|T2], [Lm|T3], [Ls|T4]) :-
    write(Count), tab(8), write(Lk), tab(8), write(Lh), tab(8),
    write(Lm), tab(8), write(Ls), nl, Count_next is Count+1,
    cetakJadwal(Count_next, T1, T2, T3, T4).


% Appendix: Fakta-fakta yang diperlukan.
% Fakta Hari:
hari("senin") :- !.
hari("selasa") :- !.
hari("rabu") :- !.
hari("kamis") :- !.
hari("jumat") :- !.
hari("sabtu") :- !.


% Fakta Jam: Hasil analisis dari jadwal kuliah keseluruhan mahasiswa UI.
jam_mulai(8.0) :- !.
jam_mulai(9.0) :- !.
jam_mulai(10.0) :- !.
jam_mulai(11.0) :- !.
jam_mulai(13.0) :- !.
jam_mulai(14.0) :- !.
jam_mulai(15.0) :- !.
jam_mulai(16.0) :- !.

% Jam selesai standar.
jam_selesai(9.0) :- !.
jam_selesai(10.0) :- !.
jam_selesai(11.0) :- !.
jam_selesai(12.0) :- !.
jam_selesai(13.0) :- !.
jam_selesai(14.0) :- !.
jam_selesai(15.0) :- !.
jam_selesai(16.0) :- !.
jam_selesai(17.0) :- !.
jam_selesai(18.0) :- !.

% Kemungkinan selesai jika kelas dimulai jam 8.
jam_selesai(8.5) :- !.
jam_selesai(9.4) :- !.
jam_selesai(10.3) :- !.

% Kemungkinan selesai jika kelas dimulai jam 9.
jam_selesai(9.5) :- !.
jam_selesai(10.4) :- !.
jam_selesai(11.3) :- !.

% Kemungkinan selesai jika kelas dimulai jam 10.
jam_selesai(10.5) :- !.
jam_selesai(11.4) :- !.
jam_selesai(12.3) :- !.

% Kemungkinan selesai jika kelas dimulai jam 11.
jam_selesai(11.5) :- !.
jam_selesai(12.4) :- !.
jam_selesai(13.3) :- !.

% Kemungkinan selesai jika kelas dimulai jam 13.
jam_selesai(13.5) :- !.
jam_selesai(14.4) :- !.
jam_selesai(15.3) :- !.

% Kemungkinan selesai jika kelas dimulai jam 14.
jam_selesai(14.5) :- !.
jam_selesai(15.4) :- !.
jam_selesai(16.3) :- !.

% Kemungkinan selesai jika kelas dimulai jam 15.
jam_selesai(15.5) :- !.
jam_selesai(16.4) :- !.
jam_selesai(17.3) :- !.

% Kemungkinan selesai jika kelas dimulai jam 16.
jam_selesai(16.5) :- !.
jam_selesai(17.4) :- !.
jam_selesai(18.3) :- !.













