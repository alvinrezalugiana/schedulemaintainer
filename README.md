SUSUNJADWAL2 (Schedule Maintainer)

Program ini bertujuan untuk membantu mahasiswa menyusun jadwal kelas. (This program helps students to maintain their class schedule)

INSTALASI (Installation)

Setelah melakukan cloning repository, buatlah sebuah virtual environment. (After cloning the repository, create a virtual environment)

Aktifkan virtual environment. (Activate the virtual environment)

Lakukan instalasi dependencies dengan command: 'pip install -r requirements.txt' (Do the installation by type the command pip install -r requirements.txt)